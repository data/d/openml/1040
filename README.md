# OpenML dataset: sylva_prior

https://www.openml.org/d/1040

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets from the Agnostic Learning vs. Prior Knowledge Challenge (http://www.agnostic.inf.ethz.ch)

**Note: Derived from the covertype dataset**

Dataset from: http://www.agnostic.inf.ethz.ch/datasets.php

Modified by TunedIT (converted to ARFF format)


SYLVA is the ecology database


The task of SYLVA is to classify forest cover types. The forest cover type for 30 x 30 meter cells is obtained from US Forest Service (USFS) Region 2 Resource Information System &#40;RIS&#41; data. We brought it back to a two-class classification problem (classifying Ponderosa pine vs. everything else). The "agnostic learning track" data consists in 216 input variables. Each pattern is composed of 4 records: 2 true records matching the target and 2 records picked at random. Thus 1/2 of the features are distracters. The "prior knowledge track" data is identical to the "agnostic learning track" data, except that the distracters are removed and the identity of the features is revealed. For that track, the forest cover original ids are revealed for training data.

Data type: non-sparse
Number of features: 108
Number of examples and check-sums:
Pos_ex Neg_ex Tot_ex Check_sum
Train   805 12281 13086 118996108.00
Valid    81  1228  1309 11904801.00

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1040) of an [OpenML dataset](https://www.openml.org/d/1040). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1040/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1040/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1040/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

